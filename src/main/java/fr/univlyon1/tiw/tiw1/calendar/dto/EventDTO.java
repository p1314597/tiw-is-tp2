package fr.univlyon1.tiw.tiw1.calendar.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import fr.univlyon1.tiw.tiw1.calendar.impl.EventImpl;

public class EventDTO implements EventImpl {

	private String title;
    private String description;
    private Date start;
    private Date end;
    private String id;
    
	public EventDTO() {
	    	
	}
	
	public EventDTO(String title, String description, Date start, Date end, String id) {
	        this.title = title;
	        this.description = description;
	        this.start = start;
	        this.end = end;
	        this.id = id;
	}
    
	@Override
	public String getTitle() {
		return this.title;
	}

	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public Date getStart() {
		return this.start;
	}

	@Override
	public void setStart(Date start) {
		this.start = start;
	}

	@Override
	public Date getEnd() {
		return this.end;
	}

	@Override
	public void setEnd(Date end) {
		this.end = end;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String formatDate(Date d) {
		return new SimpleDateFormat("dd/MM/yyyy").format(d);
	}

	@Override
	public String getInfos() {
		String infos = "Title: " + getTitle();
        infos += "\nDescription: " + getDescription();
        infos += "\nStart: " + formatDate(getStart());
        infos += "\nEnd: " + formatDate(getEnd());
        return infos;
	}

}
