package fr.univlyon1.tiw.tiw1.calendar.impl;

import java.util.Date;

public interface EventImpl {
	
	String getTitle();

    void setTitle(String title);

    String getDescription();

    void setDescription(String description);

    Date getStart();

    void setStart(Date start);

    Date getEnd();

    void setEnd(Date end);

    String getId();

    void setId(String id);
    
    String formatDate(Date d);

    String getInfos();

    boolean equals(Object o);

}