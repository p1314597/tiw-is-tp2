package tp1.serveur.action;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import tp1.serveur.dao.*;
import tp1.serveur.modele.Calendar;
import tp1.serveur.modele.Event;

public class initEvents {

	private static final Logger LOG = LoggerFactory.getLogger(Calendar.class);

	private String name;
	private ICalendarDAO dao;
	private Collection<Event> events;
	private Calendar calendar;

	public void synchronizeEvents() {
	        try {
	            Calendar tmp = dao.loadCalendar(this.name);
	            this.setEvents(tmp.getEvents());
	        } catch (CalendarNotFoundException e) {
	            LOG.error("Error while loading calendar",e );
	        }
	    }
	public Collection<Event> getEvents() {
		return events;
	}

	public void setEvents(Collection<Event> events) {
		this.events = events;
	}
}
