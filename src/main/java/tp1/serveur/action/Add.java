package tp1.serveur.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tp1.Config;
import tp1.serveur.dao.ICalendarDAO;
import tp1.serveur.modele.Calendar;
import tp1.serveur.modele.Event;

public class  Add{
	private static final Logger LOG = LoggerFactory.getLogger(Calendar.class);

    @XmlElement
    private String name;
    private ICalendarDAO dao;
    private Collection<Event> events;
    private Calendar calendar;
     
	public Event addEvent(String title, String start, String end, String description, String format) throws ParseException {
	        UUID uuid = UUID.randomUUID();
	        Event evt = new Event(title, description, parseDate(start, format), parseDate(end, format), uuid.toString());
	        events.add(evt);
	        // insertion dans le support de persistance
	        try {
	            dao.saveEvent(evt, calendar);
	            LOG.debug("DAO called in addEvent");
	        } catch (Exception e) {
	            LOG.error("Error in addEvent", e);
	        }
	        return evt;
	    }
	
	 public Calendar getCalendar() {
		return calendar;
	}

	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}

	private static Date parseDate(String s, String format) throws ParseException {
	    	Config c = new Config(format);
	        return new SimpleDateFormat(c.config.getProperty(Config.DATE_FORMAT)).parse(s);
	    }
}
