package tp1.serveur.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.xml.bind.annotation.XmlElement;

import tp1.Config;
import tp1.serveur.dao.ICalendarDAO;
import tp1.serveur.modele.Calendar;
import tp1.serveur.modele.Event;

public class listEvents {
	
	@XmlElement
	private String name;
	private ICalendarDAO dao;
	private Collection<Event> events;
	
	public Collection<Event> getEvents() {
		return events;
	}

	public void setEvents(Collection<Event> events) {
		this.events = events;
	}

	public String getInfos(String format) {
        String infos = "";
        for (Iterator<Event> i = events.iterator(); i.hasNext(); ) {
            Event temp = i.next();
            infos += temp.getInfos(format) + "\n";
        }
        return infos;
    }
    
    private static Date parseDate(String s, String format) throws ParseException {
    	Config c = new Config(format);
        return new SimpleDateFormat(c.config.getProperty(Config.DATE_FORMAT)).parse(s);
    }

}
