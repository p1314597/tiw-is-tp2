package tp1.serveur.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tp1.Config;
import tp1.serveur.dao.ICalendarDAO;
import tp1.serveur.modele.Calendar;
import tp1.serveur.modele.Event;

public class Remove {

	private static final Logger LOG = LoggerFactory.getLogger(Calendar.class);
	
	@XmlElement
	private String name;
	private ICalendarDAO dao;
	private Collection<Event> events;
	private Calendar calendar;                
	 
	 public Calendar getCalendar() {
		return calendar;
	}


	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}


	public void removeEvent(String title, String start, String end, String desc, String format) throws ParseException {
	        Event event = new Event(title, desc, parseDate(start, format), parseDate(end, format), null);

	        // Suppression dans la liste
	        for (Iterator<Event> i = events.iterator(); i.hasNext(); ) {
	            Event temp = i.next();
	            if (temp.equals(event)) {
	                events.remove(temp);
	                // Suppression dans le support de persistance
	                try {
	                    dao.deleteEvent(temp, calendar);
	                    LOG.debug("DAO called in deleteEvent");
	                } catch (Exception e) {
	                    LOG.error("Error in deleteEvent", e);
	                }
	                removeEvent(title, start, end, desc, format);
	                return;
	            }
	        }
	    }

	
	 private static Date parseDate(String s, String format) throws ParseException {
	    	Config c = new Config(format);
	        return new SimpleDateFormat(c.config.getProperty(Config.DATE_FORMAT)).parse(s);
	    }
}