package tp1.serveur;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.parameters.ConstantParameter;
import static org.picocontainer.Characteristics.CACHE;

import tp1.Config;
import tp1.serveur.dao.XMLCalendarDAO;
import tp1.client.CalendarUI;

public class ServeurImpl implements Serveur {
	
	private Calendar calendar;
	private MutablePicoContainer conteneur = new DefaultPicoContainer();
	

	public  ServeurImpl(){						
		// Calendar -> ArrayList
		conteneur.as(CACHE).addComponent(ArrayList.class);
		
		// Calendar -> XMLCalendarDAO
		// XMLCalendarDAO -> instance du fichier XML
		conteneur.as(CACHE).addComponent(XMLCalendarDAO.class, new ConstantParameter(new File(CalendarUI.getDefaultDirectory())));
		
		// Calendar -> DateFormat
		conteneur.as(CACHE).addComponent(DateFormat.class);
		
		// SimpleDateFormat -> String dans le fichier de configuration
		conteneur.as(CACHE).addComponent(new ConstantParameter(Config.DATE_FORMAT));
		
		conteneur.as(CACHE).addComponent(Calendar.class);
		
		calendar = conteneur.getComponent(Calendar.class);
		
		calendar.start();
	}
	
	public String processRequest(String commande, HashMap<String, String> parametres) {
		if(commande.equals("add")){
			//calendar.addEvent(parametres.get("title"),parametres.get("start"), parametres.get("end"),parametres.get("desc"), null);
		}
		if(commande.equals("remove")){
			//calendar.removeEvent(parametres.get("title"),parametres.get("start"), parametres.get("end"),parametres.get("desc"), null);
		}
		if(commande.equals("init")){
			//calendar.synchronizeEvents();
		}
		if(commande.equals("list")){
			//calendar.getInfos( null);
		}
    	return commande + " successfully";
	}
	
	public Calendar getCalendar() {
		return calendar;
	}
	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}
	public MutablePicoContainer getConteneur() {
		return conteneur;
	}

	public void setConteneur(MutablePicoContainer conteneur) {
		this.conteneur = conteneur;
	}
}