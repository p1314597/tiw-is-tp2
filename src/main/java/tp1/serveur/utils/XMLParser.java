package tp1.serveur.utils;

import java.util.ArrayList;

public class XMLParser {

    private String filePath;

    public XMLParser(String filePath) {
        this.filePath = filePath;
    }

    public ArrayList<String> parse() {

        //mock of an xml parsing

        ArrayList<String> classNames = new ArrayList<>();
        classNames.add("java.util.ArrayList");
        classNames.add("tp1.serveur.dao.XMLCalendarDAO");
        classNames.add("java.text.SimpleDateFormat");

        classNames.add("tp1.serveur.actions.AddEvent");
        classNames.add("tp1.serveur.actions.InitEvent");
        classNames.add("tp1.serveur.actions.ListEvents");
        classNames.add("tp1.serveur.actions.RemoveEvent");

         return classNames;
    }
}
