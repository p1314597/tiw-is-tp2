package tp1.serveur.dao;

import tp1.Config;
import tp1.serveur.dao.ICalendarDAO;
import tp1.serveur.modele.Calendar;
import tp1.serveur.modele.Event;

public interface ICalendarDAO {

    /**
     * Sauve un calendrier dans le support de persitance.
     * @param calendar le calendrier à sauver.
     */
    void saveCalendar(Calendar calendar);

    /**
     * Supprime un calendrier dans le support de persitance.
     * @param calendar le calendrier à sauver.
     */
    void deleteCalendar(Calendar calendar);

    /**
     * Charge un calendrier depuis le support de persistance.
     * @param name le nom du calendrier à charger.
     * @return le calendrier chargé.
     * @throws CalendarNotFoundException si le calendrier n'a pas été trouvé
     */
    tp1.serveur.modele.Calendar loadCalendar(String name) throws CalendarNotFoundException;

    /**
     * Sauve un evenement d'un calendrier dans le support de persitance.
     * @param evt l'evenement à sauver.
     * @param calendar le calendrier dans lequel se trouve l'evenement.
     */
    void saveEvent(tp1.serveur.modele.Event evt, tp1.serveur.modele.Calendar calendar);

    /**
     * Supprime un evenement d'un calendrier dans le support de persitance.
     * @param temp l'evenement à sauver.
     * @param calendar le calendrier dans lequel se trouve l'evenement.
     */
    void deleteEvent(tp1.serveur.modele.Event temp, tp1.serveur.modele.Calendar calendar);
}
