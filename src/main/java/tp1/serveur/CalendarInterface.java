package tp1.serveur;

import java.util.HashMap;

import org.picocontainer.Startable;

public interface CalendarInterface extends Startable{
	 String processRequest(String commande, HashMap<String, String> parametres); 
}
