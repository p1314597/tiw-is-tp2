package tp1.serveur;

import java.util.HashMap;

public interface Serveur {

	 public String processRequest(String command, HashMap<String, String> params);

}
