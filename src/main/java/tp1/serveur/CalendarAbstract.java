package tp1.serveur;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

import org.picocontainer.Startable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tp1.Config;
import tp1.serveur.dao.*;
import tp1.serveur.modele.Calendar;
import tp1.serveur.modele.Event;

public abstract class CalendarAbstract implements Startable{
	private static final Logger LOG = LoggerFactory.getLogger(Calendar.class);

    @XmlElement
    private String name;   
	private ICalendarDAO dao;
    private Collection<Event> events;

    public  CalendarAbstract() {
    }
    
    private static Date parseDate(String s, String format) throws ParseException {
    	Config c = new Config(format);
        return new SimpleDateFormat(c.config.getProperty(Config.DATE_FORMAT)).parse(s);
    }
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ICalendarDAO getDao() {
		return dao;
	}

	public void setDao(ICalendarDAO dao) {
		this.dao = dao;
	}

	public Collection<Event> getEvents() {
		return events;
	}

	public void setEvents(Collection<Event> events) {
		this.events = events;
	}
	
	@Override
	public void start() {
		// TODO Auto-generated method stub
		System.out.println("Composant Calendar démarré. Objet d'accès aux données :" + dao.toString());
		
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}
}
