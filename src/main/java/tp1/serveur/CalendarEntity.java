package tp1.serveur;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.univlyon1.tiw.tiw1.calendar.dao.ICalendarDAO;
import fr.univlyon1.tiw.tiw1.calendar.dao.XMLCalendarDAO;
import fr.univlyon1.tiw.tiw1.calendar.modele.Calendar;
import fr.univlyon1.tiw.tiw1.calendar.modele.Event;



@XmlRootElement(name = "calendar")
@XmlType(propOrder = {"name", "events"})
public class CalendarEntity {

    private static final Logger LOG = LoggerFactory.getLogger(Calendar.class);

    @XmlElement
    private String name;
    private ICalendarDAO dao;
    private Collection<Event> events;

    public CalendarEntity() {
    }

    public CalendarEntity(String name, File daoDirectory) {
        this.name = name;
        try {
            this.dao = new XMLCalendarDAO(daoDirectory);
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.events = new ArrayList<>();
    }

    @XmlElement(name="event")
    public Collection<Event> getEvents() {
        return events;
    }

    public void setEvents(Collection<Event> events) {
        this.events = events;
    }

  
	
}
