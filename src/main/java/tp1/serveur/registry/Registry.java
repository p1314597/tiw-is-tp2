package tp1.serveur.registry;

import tp1.serveur.modele.CalendarContext;
import tp1.serveur.modele.CalendarContextImpl;
import javassist.NotFoundException;

import java.util.HashMap;

public class Registry {

//registry stores contexts identified by a string
//In the end we have something like HashMap<String, HashMap<String, object>> the second hashmap being a context
    private HashMap<String, CalendarContext> reg;

    public Registry() {
        this.reg = new HashMap<>();
    }

    public boolean set(String key, Object o) {
        String[] keyParts = key.split("/");
        if(reg.containsKey(keyParts[0])) {
            reg.get(keyParts[0]).setObject(keyParts[1], o);
            return true;
        }else {
            reg.put(keyParts[0], new CalendarContextImpl());
            reg.get(keyParts[0]).setObject(keyParts[1], o);

            return true;
        }
    }

    public Object get(String key) throws NotFoundException{

        String[] keyParts = key.split("/");

        if(reg.containsKey(keyParts[0])){
            return reg.get(keyParts[0]).getObject(keyParts[1]);
        }else{
            throw new NotFoundException(key);
        }
    }
}
