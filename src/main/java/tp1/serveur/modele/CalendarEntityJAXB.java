package tp1.serveur.modele;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Collection;

@XmlRootElement(name = "calendar")
@XmlType(propOrder = {"name", "events"})
public class CalendarEntityJAXB {

    @XmlElement
    private String name;

    @XmlElement(name="event")
    private Collection<Event> events;

    public CalendarEntityJAXB() {}

    public CalendarEntityJAXB(CalendarEntity ce) {

        this.name = ce.getName();
        this.events = ce.getEvents();

    }


}
