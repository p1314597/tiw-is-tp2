package tp1.serveur.modele;

public interface CalendarContext {

    public Object getObject(String key);

    public void setObject(String key, Object o);

}
