package tp1.serveur.modele;

import tp1.Config;
import tp1.serveur.action.Add;
import tp1.serveur.action.Remove;
import tp1.serveur.action.initEvents;
import tp1.serveur.action.listEvents;
import tp1.serveur.dao.CalendarNotFoundException;
import tp1.serveur.dao.ICalendarDAO;
import tp1.serveur.dao.XMLCalendarDAO;
import tp1.serveur.dto.EventDTO;
import org.picocontainer.Startable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tp1.serveur.modele.*;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@XmlRootElement(name = "calendar")
@XmlType(propOrder = {"name", "events"})
public class Calendar implements Startable {
    //in the final version we won't have any use for this class

    private static final Logger LOG = LoggerFactory.getLogger(Calendar.class);

    @XmlElement
    private String name;
    private ICalendarDAO dao;
    private Collection<Event> events;
    private Config config;

    public Calendar() {
        this.config=new Config(null);
    }

    public Calendar(String name, File daoDirectory, Config config) {

        this.name = name;
        this.config = config;
        try {
            this.dao = new XMLCalendarDAO(daoDirectory);
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.events = new ArrayList<>();
    }

    @XmlElement(name="event")
    public Collection<Event> getEvents() {
        return events;
    }

    public void setEvents(Collection<Event> events) {
        this.events = events;
    }

    private static Date parseDate(String s, String format) throws ParseException {
    	Config c = new Config(format);
        return new SimpleDateFormat(c.config.getProperty(Config.DATE_FORMAT)).parse(s);
    }

    public String processRequest(String commande, HashMap<String, String> parametres){
		if(commande.equals("add")){
			Add addObject = new Add();
			addObject.setCalendar(this);
			
			try {
				 addObject.addEvent(parametres.get("title"),parametres.get("start"),
						parametres.get("end"),parametres.get("desc"), null);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(commande.equals("remove")){
			Remove removeObject = new Remove();
			removeObject.setCalendar(this);
			try {
				removeObject.removeEvent(parametres.get("title"),parametres.get("start"),
						parametres.get("end"),parametres.get("desc"), null);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(commande.equals("init")){
			initEvents init = new initEvents();
			init.setEvents(events);
			init.synchronizeEvents();
		}
		if(commande.equals("list")){
			listEvents list = new listEvents();
			list.setEvents(events);
			list.getInfos( null);
		}
    	return commande + " successfully";   	
    }

    private Event addEvent(String title, String start, String end, String description, String format) throws ParseException {
        UUID uuid = UUID.randomUUID();
        Event evt = new Event(title, description, parseDate(start, format), parseDate(end, format), uuid.toString());
        events.add(evt);
        // insertion dans le support de persistance
        try {
            dao.saveEvent(evt, this);
            LOG.debug("DAO called in addEvent");
        } catch (Exception e) {
            LOG.error("Error in addEvent", e);
        }
        return evt;
    }
    
    public Event addEventDTO(String title, String start, String end, String description, String format) throws ParseException {
        UUID uuid = UUID.randomUUID();
        EventDTO evtDTO = new EventDTO(title, description, parseDate(start, format), parseDate(end, format), uuid.toString());
        
        Event evt = new Event(evtDTO.getTitle() ,evtDTO.getDescription(), evtDTO.getStart(), evtDTO.getEnd(), uuid.toString());
        events.add(evt);
        // insertion dans le support de persistance
        try {
            dao.saveEvent(evt, this);
            LOG.debug("DAO called in addEvent");
        } catch (Exception e) {
            LOG.error("Error in addEvent", e);
        }
        return evt;
    }

    private void removeEvent(String title, String start, String end, String desc, String format) throws ParseException {
        Event event = new Event(title, desc, parseDate(start, format), parseDate(end, format), null);

        // Suppression dans la liste
        for (Iterator<Event> i = events.iterator(); i.hasNext(); ) {
            Event temp = i.next();
            if (temp.equals(event)) {
                events.remove(temp);
                // Suppression dans le support de persistance
                try {
                    dao.deleteEvent(temp, this);
                    LOG.debug("DAO called in deleteEvent");
                } catch (Exception e) {
                    LOG.error("Error in deleteEvent", e);
                }
                removeEvent(title, start, end, desc, format);
                return;
            }
        }
    }

    public void removeEventDTO(String title, String start, String end, String desc, String format) throws ParseException {
    	EventDTO evtDTO = new EventDTO(title, desc, parseDate(start, format), parseDate(end, format), null);
    	Event event = new Event(evtDTO.getTitle() ,evtDTO.getDescription(), evtDTO.getStart(), evtDTO.getEnd(), null);
        // Suppression dans la liste
        for (Iterator<Event> i = events.iterator(); i.hasNext(); ) {
            Event temp = i.next();
            if (temp.equals(event)) {
                events.remove(temp);
                // Suppression dans le support de persistance
                try {
                    dao.deleteEvent(temp, this);
                    LOG.debug("DAO called in deleteEvent");
                } catch (Exception e) {
                    LOG.error("Error in deleteEvent", e);
                }
                removeEvent(title, start, end, desc, format);
                return;
            }
        }
    }
    
    private void synchronizeEvents() {
        try {
            Calendar tmp = dao.loadCalendar(this.name);
            this.setEvents(tmp.getEvents());
        } catch (CalendarNotFoundException e) {
            LOG.error("Error while loading calendar",e );
        }
    }

    public String getName() {
        return this.name;
    }


    /**
     * Méthode destinée à l'affichage
     *
     * @return une string formattée contentant toutes les infos des événements de l'calendar
     */
    public String getInfos() {
        String infos = "";
        for (Iterator<Event> i = events.iterator(); i.hasNext(); ) {
            Event temp = i.next();
            infos += temp.getInfos(infos) + "\n";
        }
        return infos;
    }

    @Override
    public void start() {
        LOG.info("Calendar DAO démarré "+this.dao.toString());
    }

    @Override
    public void stop() {
        LOG.info("Calendar DAO arrêté "+this.dao.toString());
    }
}