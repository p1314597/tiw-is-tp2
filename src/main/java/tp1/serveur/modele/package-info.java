@XmlSchema(elementFormDefault = XmlNsForm.QUALIFIED, namespace = "http://master-info.univ-lyon1.fr/TIW/TIW1/calendar")

package tp1.serveur.modele;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;