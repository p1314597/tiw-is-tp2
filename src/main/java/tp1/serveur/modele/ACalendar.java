package tp1.serveur.modele;

import tp1.Config;
import tp1.serveur.dao.ICalendarDAO;
import tp1.serveur.registry.Registry;

import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlElement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

public abstract class ACalendar {

    protected static final Logger LOG = LoggerFactory.getLogger(Calendar.class);

    protected String name;
    protected ICalendarDAO dao;
    protected Collection<Event> events;
    protected Config config;

    public ACalendar() {

    }

    public ACalendar(String name, Registry reg, Collection<Event> events, Config config) {
        this.name = name;
        try {
            this.dao = (ICalendarDAO) reg.get("dao/xmldao");
        }catch(NotFoundException e) {
            LOG.error("dao not created");
        }
        this.events = events;
        this.config = config;
    }
    private static Date parseDate(String s, String format) throws ParseException {
    	Config c = new Config(format);
        return new SimpleDateFormat(c.config.getProperty(Config.DATE_FORMAT)).parse(s);
    }
}
