package tp1.serveur.modele;

import org.picocontainer.Startable;

import java.util.HashMap;

public interface ICalendar extends Startable {

    public boolean processRequest(HashMap<String,String> params);
}
