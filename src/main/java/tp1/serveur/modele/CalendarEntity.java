package tp1.serveur.modele;

import tp1.Config;
import tp1.serveur.dao.ICalendarDAO;

import java.util.Collection;

public class CalendarEntity {

    private String name;
    private Collection<Event> events;
    private Config config;

    public CalendarEntity(String name, Collection<Event> events, Config config) {
        this.name = name;
        this.events = events;
        this.config = config;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Event> getEvents() {
        return events;
    }

    public void setEvents(Collection<Event> events) {
        this.events = events;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }
}
