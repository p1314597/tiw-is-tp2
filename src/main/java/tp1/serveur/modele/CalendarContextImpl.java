package tp1.serveur.modele;

import tp1.serveur.dao.ICalendarDAO;

import java.util.HashMap;
import java.util.NoSuchElementException;

public class CalendarContextImpl implements CalendarContext {

    public HashMap<String, Object> context;

    public CalendarContextImpl() {
        context = new HashMap<>();
    }

    @Override
    public Object getObject(String key) throws NoSuchElementException{

        if(context.containsKey(key)) {
            return context.get(key);
        }
        throw new NoSuchElementException();
    }

    @Override
    public void setObject(String key, Object o) {
        context.put(key, o);
    }
}
