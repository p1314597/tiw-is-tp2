package tp1.serveur.modele;

import tp1.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@XmlType(propOrder = {"title", "start", "end", "description"})
@XmlSeeAlso({CalendarEntityJAXB.class})
public class Event {

    private static final Logger LOG = LoggerFactory.getLogger(Calendar.class);

    private String title;
    private String description;
    private Date start;
    private Date end;
    private String id;
    private Config config;

    public Event() {
        LOG.error("ERREUR");
    }

    public Event(String title, String description, Date start, Date end, String id) {
        this.title = title;
        this.description = description;
        this.start = start;
        this.end = end;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @XmlAttribute
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private static Date parseDate(String s, String format) throws ParseException {
    	Config c = new Config(format);
        return new SimpleDateFormat(c.config.getProperty(Config.DATE_FORMAT)).parse(s);
    }

    public String getInfos(String format) {
        String infos = "Title: " + getTitle();
        infos += "\nDescription: " + getDescription();
        infos += "\nStart: " + formatDate(getStart(), format);
        infos += "\nEnd: " + formatDate(getEnd(), format);
        return infos;
    }
    private String formatDate(Date d, String format) {
    	Config c = new Config(format);
        return new SimpleDateFormat(c.config.getProperty(Config.DATE_FORMAT)).format(d);
    }

    // Pour la comparaison dans la suppression
    //On approxime les dates à la précision du format près, ça suffit bien...
    
    public boolean equals(Object o, String format) {
        Event e = (Event) o;
        return this.title.equals(e.title) && this.description.equals(e.description)
                && formatDate(this.start, format).equals(formatDate(e.start, format)) && formatDate(this.end, format).equals(formatDate(e.end, format));
    }
}
