package tp1;

import java.util.Properties;

public class Config {
	public Properties config = new Properties();
	public static final String DATE_FORMAT = "date_format";
	private String format;

	public Config(String format) {
		// defaultCfg.setProperty(DATE_FORMAT,"EEE MMM dd kk:mm:ss zzz yyyy");
		this.format = format;
		config.setProperty(DATE_FORMAT, format);
	}

	public String getFormat() {
		return format;
	}
}
